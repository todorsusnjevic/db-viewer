package com.ataccama.exam.dbviewer.vo;

public enum TableStatisticType {
    MIN, MAX, AVG, MEDIAN, NUMBER_OF_RECORDS, NUMBER_OF_ATTRIBUTES
}
