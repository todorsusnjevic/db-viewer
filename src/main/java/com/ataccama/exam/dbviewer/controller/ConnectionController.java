package com.ataccama.exam.dbviewer.controller;

import java.util.List;

import com.ataccama.exam.dbviewer.model.Connection;
import com.ataccama.exam.dbviewer.service.ConnectionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("connections")
public class ConnectionController {
    @Autowired
    private ConnectionService connectionService;

    @GetMapping("{id}")
    public Connection get(@PathVariable Long id) {
        return connectionService.get(id);
    }

    @GetMapping()
    public List<Connection> getAll() {
        return connectionService.getAll();
    }

    @PostMapping()
    public Connection save(@RequestBody Connection connection) {
        return connectionService.save(connection);
    }

    @PutMapping()
    public void update(@RequestBody Connection connection) {
        connectionService.update(connection);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        connectionService.delete(id);
    }
}
