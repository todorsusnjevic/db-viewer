package com.ataccama.exam.dbviewer.controller;

import java.util.List;

import com.ataccama.exam.dbviewer.dto.ColumnDto;
import com.ataccama.exam.dbviewer.dto.RawDataDto;
import com.ataccama.exam.dbviewer.dto.SchemaDto;
import com.ataccama.exam.dbviewer.dto.TableDto;
import com.ataccama.exam.dbviewer.dto.TableStatisticsDto;
import com.ataccama.exam.dbviewer.service.SchemaService;
import com.ataccama.exam.dbviewer.service.StatisticService;
import com.ataccama.exam.dbviewer.vo.TableStatisticType;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("schema")
public class SchemaController {
    @Autowired
    private SchemaService schemaService;

    @Autowired
    private StatisticService statisticService;

    @GetMapping
    public List<SchemaDto> getSchemas(@RequestParam Long connectionId) {
        return schemaService.getSchemas(connectionId);
    }

    @GetMapping("{schemaName}/tables")
    public List<TableDto> getTables(@RequestParam Long connectionId, @PathVariable String schemaName) {
        return schemaService.getTables(connectionId, schemaName);
    }

    @GetMapping("{schemaName}/tables/{tableName}/columns")
    public List<ColumnDto> getColumns(@RequestParam Long connectionId, @PathVariable String schemaName, @PathVariable String tableName) {
        return schemaService.getColumns(connectionId, schemaName, tableName);
    }

    @GetMapping("{schemaName}/tables/{tableName}/statistics")
    public List<TableStatisticsDto> getStatistic(@RequestParam Long connectionId, @PathVariable String schemaName, @PathVariable String tableName, @RequestParam TableStatisticType tableStatisticType) throws NotImplementedException {
        switch (tableStatisticType) {
            case AVG:
            case MAX:
            case MIN:
                return statisticService.operation(connectionId, schemaName, tableName, tableStatisticType);
            case MEDIAN:
                return statisticService.median(connectionId, schemaName, tableName);
            case NUMBER_OF_ATTRIBUTES:
                return statisticService.numberOfAttributes(connectionId, schemaName, tableName);
            case NUMBER_OF_RECORDS:
                return statisticService.numberOfRecords(connectionId, schemaName, tableName);
            default:
                throw new NotImplementedException("Operation not supported");
        }
    }

    @GetMapping("{schemaName}/tables/{tableName}/data")
    public List<RawDataDto> getData(@RequestParam Long connectionId, @PathVariable String schemaName, @PathVariable String tableName) {
        return schemaService.getData(connectionId, schemaName, tableName);
    }
}
