package com.ataccama.exam.dbviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Db viewer API", version = "1.0", description = "Database Information"))
public class AtaccammaApplication {
    public static void main(String[] args) {
        SpringApplication.run(AtaccammaApplication.class, args);
    }
}
