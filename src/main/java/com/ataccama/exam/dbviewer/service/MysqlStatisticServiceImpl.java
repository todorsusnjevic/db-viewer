package com.ataccama.exam.dbviewer.service;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ataccama.exam.dbviewer.dto.TableStatisticsDto;
import com.ataccama.exam.dbviewer.exceptions.ConnectionException;
import com.ataccama.exam.dbviewer.model.Connection;
import com.ataccama.exam.dbviewer.vo.TableStatisticType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MysqlStatisticServiceImpl implements StatisticService {
    @Autowired
    private ConnectionService connectionService;

    @Override
    public List<TableStatisticsDto> operation(Long connectionId, String schemaName, String tableName,
                                              TableStatisticType tableStatisticType) throws ConnectionException {
        final List<TableStatisticsDto> result = new ArrayList<>();
        final Connection connParams = connectionService.get(connectionId);
        try (java.sql.Connection dbConnection =
                     DriverManager.getConnection(buildConnectionUrl(connParams, schemaName),
                             connParams.getUsername(),
                             connParams.getPassword());
             Statement stmt = dbConnection.createStatement();) {
            String query = buildFirstRecordSql(schemaName, tableName);
            ResultSet resultSet = stmt.executeQuery(query);
            final ResultSetMetaData metaData = resultSet.getMetaData();
            final int columnCount = metaData.getColumnCount();
            for (int i = 1; i <= columnCount; i++) {
                String sql = buildSql(schemaName, tableName, metaData, i, tableStatisticType);
                ResultSet rs = stmt.executeQuery(sql);
                rs.next();
                result.add(new TableStatisticsDto(metaData.getColumnLabel(i), tableStatisticType, rs.getString(buildLabel(tableStatisticType))));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConnectionException(connParams);
        }
        return result;
    }

    @Override
    public List<TableStatisticsDto> median(Long connectionId, String schemaName, String tableName) throws ConnectionException {
        final List<TableStatisticsDto> result;
        final Map<String, List<Object>> nameValuesMap = new HashMap<>();
        final Map<String, Integer> nameTypeMap = new HashMap<>();
        final Connection connParams = connectionService.get(connectionId);
        try (java.sql.Connection dbConnection =
                     DriverManager.getConnection(buildConnectionUrl(connParams, schemaName),
                             connParams.getUsername(),
                             connParams.getPassword());
             Statement stmt = dbConnection.createStatement();) {
            String sql = "SELECT * FROM " + schemaName + "." + tableName;
            ResultSet rs = stmt.executeQuery(sql);
            final ResultSetMetaData metaData = rs.getMetaData();
            final int columnCount = metaData.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    final List<Object> objects = nameValuesMap.getOrDefault(metaData.getColumnLabel(i), new ArrayList<>());
                    objects.add(rs.getObject(i));
                    nameTypeMap.put(metaData.getColumnLabel(i), metaData.getColumnType(i));
                    nameValuesMap.put(metaData.getColumnLabel(i), objects);
                }
            }
            result = calculateMedian(nameValuesMap, nameTypeMap);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConnectionException(connParams);
        }
        return result;
    }

    @Override
    public List<TableStatisticsDto> numberOfRecords(Long connectionId, String schemaName, String tableName) throws ConnectionException {
        final List<TableStatisticsDto> result = new ArrayList<>();
        final Connection connParams = connectionService.get(connectionId);
        try (java.sql.Connection dbConnection =
                     DriverManager.getConnection(buildConnectionUrl(connParams, schemaName),
                             connParams.getUsername(),
                             connParams.getPassword());
             Statement stmt = dbConnection.createStatement()) {
            String sql = "SELECT COUNT(*) as count FROM " + schemaName + "." + tableName;
            ResultSet rs = stmt.executeQuery(sql);
            rs.next();
            result.add(new TableStatisticsDto(tableName, TableStatisticType.NUMBER_OF_RECORDS, rs.getString("count")));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConnectionException(connParams);
        }
        return result;
    }

    @Override
    public List<TableStatisticsDto> numberOfAttributes(Long connectionId, String schemaName, String tableName) throws ConnectionException {
        final List<TableStatisticsDto> result = new ArrayList<>();
        final Connection connParams = connectionService.get(connectionId);
        try (java.sql.Connection dbConnection =
                     DriverManager.getConnection(buildConnectionUrl(connParams, schemaName),
                             connParams.getUsername(),
                             connParams.getPassword());
             Statement stmt = dbConnection.createStatement()) {
            String sql = buildFirstRecordSql(schemaName, tableName);
            ResultSet rs = stmt.executeQuery(sql);
            final ResultSetMetaData metaData = rs.getMetaData();
            result.add(new TableStatisticsDto(tableName, TableStatisticType.NUMBER_OF_ATTRIBUTES, metaData.getColumnCount()));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConnectionException(connParams);
        }
        return result;
    }

    private String buildConnectionUrl(Connection connParams, String schemaName) {
        return String.format("jdbc:mysql://%s:%s/%s", connParams.getHostname(), connParams.getPort(), schemaName);
    }

    private String buildSql(String schemaName, String tableName, ResultSetMetaData metaData, int i,
                            TableStatisticType tableStatisticType) throws SQLException {
        switch (tableStatisticType) {
            case MAX:
                return "SELECT MAX(`" + metaData.getColumnLabel(i) + "`) as max FROM " + schemaName + "." + tableName;
            case MIN:
                return "SELECT MIN(`" + metaData.getColumnLabel(i) + "`) as min FROM " + schemaName + "." + tableName;
            case AVG:
                return "SELECT AVG(`" + metaData.getColumnLabel(i) + "`) as avg FROM " + schemaName + "." + tableName;
            default:
                return "";
        }
    }

    private String buildFirstRecordSql(String schemaName, String tableName) {
        return String.format("SELECT * FROM %s.%s limit 1", schemaName, tableName);
    }

    private String buildLabel(TableStatisticType tableStatisticType) {
        switch (tableStatisticType) {
            case MAX:
                return "max";
            case MIN:
                return "min";
            case AVG:
                return "avg";
            default:
                return "";
        }
    }

    private List<TableStatisticsDto> calculateMedian(Map<String, List<Object>> nameValuesMap, Map<String, Integer> nameTypeMap) {
        List<TableStatisticsDto> result = new ArrayList<>();
        nameTypeMap.forEach((column, type) ->
                result.add(new TableStatisticsDto(column, TableStatisticType.MEDIAN, calculateMedian(nameValuesMap.get(column)))));
        return result;
    }

    private Object calculateMedian(List<Object> values) {
        final List<Object> sorted = values.stream().sorted().collect(Collectors.toList());

        if (sorted.size() > 1 && sorted.size() % 2 == 1) {
            return sorted.get(sorted.size() / 2);
        } else if (sorted.size() > 1) {
            Object first = sorted.get(sorted.size() / 2 - 1);
            Object second = sorted.get(sorted.size() / 2);
            if (first instanceof Long) {
                return ((Long) first + (Long) second) / 2;
            } else if (first instanceof Integer) {
                return ((Integer) first + (Integer) second) / 2;
            } else if (first instanceof Double) {
                return ((Double) first + (Double) second) / 2;
            } else {
                return null;
            }
        } else {
            return sorted.get(0);
        }
    }
}
