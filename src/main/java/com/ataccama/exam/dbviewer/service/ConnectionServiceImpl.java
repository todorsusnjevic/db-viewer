package com.ataccama.exam.dbviewer.service;

import java.util.List;

import com.ataccama.exam.dbviewer.exceptions.EntityNotFoundException;
import com.ataccama.exam.dbviewer.model.Connection;
import com.ataccama.exam.dbviewer.repository.ConnectionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConnectionServiceImpl implements ConnectionService {
    @Autowired
    private ConnectionRepository connectionRepository;

    @Override
    public Connection save(Connection connection) {
        return connectionRepository.save(connection);
    }

    @Override
    public Connection get(Long id) {
        return connectionRepository
                .findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Connection.class, "id", id.toString()));
    }

    @Override
    public List<Connection> getAll() {
        return connectionRepository.findAll();
    }

    @Override
    public void update(Connection connection) {
        connectionRepository.save(connection);
    }

    @Override
    public void delete(Long id) {
        connectionRepository.deleteById(id);
    }
}
