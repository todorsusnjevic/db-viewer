package com.ataccama.exam.dbviewer.service;

import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ataccama.exam.dbviewer.dto.ColumnDto;
import com.ataccama.exam.dbviewer.dto.RawDataDto;
import com.ataccama.exam.dbviewer.dto.SchemaDto;
import com.ataccama.exam.dbviewer.dto.TableDto;
import com.ataccama.exam.dbviewer.exceptions.ConnectionException;
import com.ataccama.exam.dbviewer.model.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MySqlSchemaServiceImpl implements SchemaService {
    private static final String TABLE_CAT_LABEL = "TABLE_CAT";
    private static final int TABLE_NAME_INDEX = 3;
    private static final int COLUMN_NAME_INDEX = 4;
    @Autowired
    private ConnectionService connectionService;

    @Override
    public List<SchemaDto> getSchemas(Long connectionId) throws ConnectionException {
        final List<SchemaDto> result = new ArrayList<>();
        final Connection connParams = connectionService.get(connectionId);
        try (java.sql.Connection dbConnection =
                     DriverManager.getConnection(buildConnectionUrl(connParams),
                             connParams.getUsername(),
                             connParams.getPassword())) {
            ResultSet rs = dbConnection.getMetaData().getCatalogs();
            while (rs.next()) {
                result.add(new SchemaDto(rs.getString(TABLE_CAT_LABEL)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConnectionException(connParams);
        }
        return result;
    }

    @Override
    public List<TableDto> getTables(Long connectionId, String schemaName) throws ConnectionException {
        final List<TableDto> result = new ArrayList<>();
        final Connection connParams = connectionService.get(connectionId);
        try (java.sql.Connection dbConnection =
                     DriverManager.getConnection(buildConnectionUrl(connParams, schemaName),
                             connParams.getUsername(),
                             connParams.getPassword())) {
            DatabaseMetaData dbMetaData = dbConnection.getMetaData();
            ResultSet rs = dbMetaData.getTables(null, null, null, null);
            while (rs.next()) {
                result.add(new TableDto(rs.getString(TABLE_NAME_INDEX)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConnectionException(connParams);
        }
        return result;
    }

    @Override
    public List<ColumnDto> getColumns(Long connectionId, String schemaName, String tableName) throws ConnectionException {
        final List<ColumnDto> result = new ArrayList<>();
        final Connection connParams = connectionService.get(connectionId);
        try (java.sql.Connection dbConnection =
                     DriverManager.getConnection(buildConnectionUrl(connParams, schemaName),
                             connParams.getUsername(),
                             connParams.getPassword())) {
            DatabaseMetaData dbMetaData = dbConnection.getMetaData();
            ResultSet rs = dbMetaData.getColumns(null, null, tableName, null);
            while (rs.next()) {
                result.add(new ColumnDto(rs.getString(COLUMN_NAME_INDEX)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConnectionException(connParams);
        }
        return result;
    }

    @Override
    public List<RawDataDto> getData(Long connectionId, String schemaName, String tableName) throws ConnectionException {
        final List<RawDataDto> result = new ArrayList<>();
        final Map<String, Object> nameValueMap = new HashMap<>();
        final Connection connParams = connectionService.get(connectionId);
        try (java.sql.Connection dbConnection =
                     DriverManager.getConnection(buildConnectionUrl(connParams, schemaName),
                             connParams.getUsername(),
                             connParams.getPassword());
             Statement stmt = dbConnection.createStatement()) {
            String sqlQuery = String.format("SELECT * FROM %s.%s limit 10", schemaName, tableName);
            ResultSet rs = stmt.executeQuery(sqlQuery);
            final ResultSetMetaData metaData = rs.getMetaData();
            final int columnCount = metaData.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    nameValueMap.put(metaData.getColumnLabel(i), rs.getObject(i));
                }
                result.add(new RawDataDto(nameValueMap));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConnectionException(connParams);
        }
        return result;
    }

    private String buildConnectionUrl(Connection connParams) {
        return String.format("jdbc:mysql://%s:%s", connParams.getHostname(), connParams.getPort());
    }

    private String buildConnectionUrl(Connection connParams, String schemaName) {
        return String.format("jdbc:mysql://%s:%s/%s", connParams.getHostname(), connParams.getPort(), schemaName);
    }
}
