package com.ataccama.exam.dbviewer.service;

import java.util.List;

import com.ataccama.exam.dbviewer.model.Connection;

public interface ConnectionService {
    Connection save(Connection connection);

    Connection get(Long id);

    List<Connection> getAll();

    void update(Connection connection);

    void delete(Long id);
}
