package com.ataccama.exam.dbviewer.service;

import java.util.List;

import com.ataccama.exam.dbviewer.dto.ColumnDto;
import com.ataccama.exam.dbviewer.dto.RawDataDto;
import com.ataccama.exam.dbviewer.dto.SchemaDto;
import com.ataccama.exam.dbviewer.dto.TableDto;
import com.ataccama.exam.dbviewer.exceptions.ConnectionException;

public interface SchemaService {
    List<SchemaDto> getSchemas(Long connectionId) throws ConnectionException;

    List<TableDto> getTables(Long connectionId, String schemaName) throws ConnectionException;

    List<ColumnDto> getColumns(Long connectionId, String schemaName, String tableName) throws ConnectionException;

    List<RawDataDto> getData(Long connectionId, String schemaName, String tableName) throws ConnectionException;
}
