package com.ataccama.exam.dbviewer.service;

import java.util.List;

import com.ataccama.exam.dbviewer.dto.TableStatisticsDto;
import com.ataccama.exam.dbviewer.exceptions.ConnectionException;
import com.ataccama.exam.dbviewer.vo.TableStatisticType;

public interface StatisticService {
    List<TableStatisticsDto> operation(Long connectionId, String schemaName, String tableName, TableStatisticType tableStatisticType) throws ConnectionException;

    List<TableStatisticsDto> median(Long connectionId, String schemaName, String tableName) throws ConnectionException;

    List<TableStatisticsDto> numberOfRecords(Long connectionId, String schemaName, String tableName) throws ConnectionException;

    List<TableStatisticsDto> numberOfAttributes(Long connectionId, String schemaName, String tableName) throws ConnectionException;
}
