package com.ataccama.exam.dbviewer.exceptions;

import com.ataccama.exam.dbviewer.model.Connection;

public class ConnectionException extends RuntimeException {

    public ConnectionException(Connection connection) {
        super(ConnectionException.generateMessage(connection));
    }

    private static String generateMessage(Connection connection) {
        return "Error using connection " + connection;
    }
}
