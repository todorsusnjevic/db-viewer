package com.ataccama.exam.dbviewer.config;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Thread local context that keeps requestId and the timestamp when the request
 * was initiated.
 */
public class RequestContext {

    private static final String X_UNIQUE_ID = "x-unique-id";
    private static final String TIMESTAMP = "timestamp";

    private static final ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<>();

    public static void initializeDurationTimer() {
        put(TIMESTAMP, System.currentTimeMillis());
    }

    /**
     * Get duration from using timestamp
     *
     * @return
     */
    public static Long getDuration() {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            return null;
        }
        Object startedTimeMillis = map.get(TIMESTAMP);
        if (startedTimeMillis == null) {
            return null;
        }
        return System.currentTimeMillis() - (Long) startedTimeMillis;
    }

    /**
     * Get current request id
     *
     * @return
     */
    public static String getRequestId() {
        Object value = get(X_UNIQUE_ID);
        if (value == null) {
            String requestId = UUID.randomUUID().toString();
            put(X_UNIQUE_ID, requestId);
            return requestId;
        }
        return value.toString();
    }

    private static void put(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<>();
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    private static Object get(String key) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            return null;
        }
        return map.get(key);
    }

    /**
     * Clean thread local context
     */
    public static void clean() {
        Map<String, Object> map = threadLocal.get();
        if (map != null) {
            map.clear();
        }
        threadLocal.remove();
    }
}