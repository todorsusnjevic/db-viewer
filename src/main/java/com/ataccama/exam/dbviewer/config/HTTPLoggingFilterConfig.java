package com.ataccama.exam.dbviewer.config;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Configuration for HTTP request logging filter.
 */
@Configuration
public class HTTPLoggingFilterConfig {

    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    /**
     * HTTP request logging filter chain.
     *
     * @return the filter registration bean
     */
    @Bean
    public FilterRegistrationBean httpRequestLoggingFilterChain() {
        FilterRegistrationBean registration = new FilterRegistrationBean(new HttpRequestLoggingFilter());
        Set<String> apiURLs = getRestMethodControllerPaths();
        registration.addUrlPatterns(apiURLs.toArray(new String[0]));
        registration.setOrder(0);
        return registration;
    }

    private Set<String> getRestMethodControllerPaths() {
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();
        return handlerMethods.entrySet().stream().map(item -> {
            HandlerMethod method = item.getValue();
            if (method.getBeanType().getName().contains("com.ataccama.controller")) {
                RequestMappingInfo mapping = item.getKey();
                assert mapping.getPatternsCondition() != null;
                return mapping.getPatternsCondition().getPatterns();
            }
            return null;
        }).filter(Objects::nonNull).map(pathSet -> pathSet.stream().map(path -> {
            if (!path.contains("{")) {
                return path;
            }
            return path.substring(0, path.indexOf("{")).concat("*");
        }).collect(Collectors.toSet())).flatMap(Set::stream).collect(Collectors.toSet());
    }

    @Slf4j
    public static class HttpRequestLoggingFilter extends OncePerRequestFilter {

        @Override
        public void doFilterInternal(HttpServletRequest httpServletRequest,
                                     HttpServletResponse httpServletResponse,
                                     FilterChain filterChain) throws ServletException, IOException {
            if (isAsyncDispatch(httpServletRequest) || isAsyncStarted(httpServletRequest)) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
                return;
            }

            RequestContext.initializeDurationTimer();

            HttpServletRequest requestWrapper = httpServletRequest;
            if (!(httpServletRequest instanceof ContentCachingRequestWrapper)) {
                requestWrapper = new ContentCachingRequestWrapper(httpServletRequest);
            }

            HttpServletResponse responseWrapper = httpServletResponse;
            if (!(httpServletResponse instanceof ContentCachingResponseWrapper)) {
                responseWrapper = new ContentCachingResponseWrapper(httpServletResponse);
            }

            log.info(createRequestLog(requestWrapper));
            try {
                filterChain.doFilter(requestWrapper, responseWrapper);
            } finally {
                String requestBody = getRequestBody(requestWrapper);
                String responseBody = getResponseBody(responseWrapper);
                log.info(createResponseLog(requestWrapper, responseWrapper, requestBody, responseBody));
                RequestContext.clean();
            }
        }

        private String getRequestBody(HttpServletRequest requestWrapper) throws UnsupportedEncodingException {
            ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(requestWrapper,
                    ContentCachingRequestWrapper.class);
            if (wrapper == null) {
                return null;
            }
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length == 0) {
                return null;
            }
            return new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
        }

        private String getResponseBody(HttpServletResponse responseWrapper) throws IOException {
            ContentCachingResponseWrapper wrapper = WebUtils.getNativeResponse(responseWrapper,
                    ContentCachingResponseWrapper.class);
            if (wrapper == null) {
                return null;
            }
            if (MediaType.APPLICATION_OCTET_STREAM_VALUE.equalsIgnoreCase(wrapper.getContentType())) {
                wrapper.copyBodyToResponse();
                return null;
            }
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length == 0) {
                wrapper.copyBodyToResponse();
                return null;
            }
            String responseBody = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
            wrapper.copyBodyToResponse();
            return responseBody;
        }

        private String createRequestLog(HttpServletRequest requestWrapper) {
            StringBuilder log = new StringBuilder();
            log.append("\nREQUEST: ").append(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()));
            log.append("\nRequestId: ").append(RequestContext.getRequestId());
            log.append("\nAddress: ").append(requestWrapper.getRequestURL());
            if (StringUtils.isNotBlank(requestWrapper.getQueryString())) {
                log.append("?").append(requestWrapper.getQueryString());
            }
            log.append("\nHttp-Method: ").append(requestWrapper.getMethod());
            log.append("\nContent-Type : ").append(requestWrapper.getContentType());
            if (StringUtils.isNotBlank(requestWrapper.getRemoteAddr())) {
                log.append("\nClient=").append(requestWrapper.getRemoteAddr());
            }
            HttpSession session = requestWrapper.getSession(false);
            if (session != null) {
                log.append("\nSession=").append(session.getId());
            }
            if (StringUtils.isNotBlank(requestWrapper.getRemoteUser())) {
                log.append("\nUser=").append(requestWrapper.getRemoteUser());
            }
            log.append("\nHeaders: ").append(Collections.list(requestWrapper.getHeaderNames()).stream()
                    .collect(Collectors.toMap(headerName -> headerName,
                            requestWrapper::getHeader)));
            return log.toString();
        }

        private String createResponseLog(HttpServletRequest requestWrapper, HttpServletResponse responseWrapper, String requestBody, String responseBody) {
            StringBuilder log = new StringBuilder();
            log.append("\nRESPONSE: ").append(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()));
            log.append("\nRequestId: ").append(RequestContext.getRequestId());
            log.append("\nDuration: ").append(String.valueOf(RequestContext.getDuration()).concat(" ms"));
            log.append("\nAddress: ").append(requestWrapper.getRequestURL());
            if (StringUtils.isNotBlank(requestWrapper.getQueryString())) {
                log.append("?").append(requestWrapper.getQueryString());
            }
            log.append("\nResponse-Code: ").append(responseWrapper.getStatus());
            log.append("\nContent-Type: ").append(responseWrapper.getContentType());
            log.append("\nHeaders: ").append(responseWrapper.getHeaderNames().stream()
                    .collect(Collectors.toMap(
                            headerName -> headerName,
                            responseWrapper::getHeader,
                            (headerName1, headerName2) -> headerName1)));
            if (StringUtils.isNotBlank(requestBody)) {
                log.append("\nRequestBody: ").append(requestBody);
            }
            if (StringUtils.isNotBlank(responseBody)) {
                log.append("\nResponseBody: ").append(responseBody);
            }
            return log.toString();
        }
    }
}
