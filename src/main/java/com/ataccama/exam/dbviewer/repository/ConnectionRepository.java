package com.ataccama.exam.dbviewer.repository;

import java.util.List;

import com.ataccama.exam.dbviewer.model.Connection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionRepository extends CrudRepository<Connection, Long> {

    @Override
    List<Connection> findAll();
}
