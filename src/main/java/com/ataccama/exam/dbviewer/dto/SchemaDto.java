package com.ataccama.exam.dbviewer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SchemaDto {
    String name;
}
