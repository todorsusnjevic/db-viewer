package com.ataccama.exam.dbviewer.dto;

import com.ataccama.exam.dbviewer.vo.TableStatisticType;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TableStatisticsDto {
    private String name;
    private TableStatisticType tableStatisticType;
    private Object value;
}
