package com.ataccama.exam.dbviewer.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RawDataDto {

    @JsonProperty("raw")
    private Map<String, Object> columnNameValueMap;
}
